//
//  ViewController.swift
//  Para ae!
//
//  Created by Danilo Altheman on 8/21/15.
//  Copyright © 2015 Quaddro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var redView = UIView()
    var blueView = UIView()
//    var timer: NSTimer!
    
    var levelSpeed = 3.5
    var clickCount = 0
    var lives = 5
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var livesLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let redSize: CGFloat = 100
        let blueSize: CGFloat = 80
        
        redView = UIView(frame: CGRect(x: 0, y: 0, width: redSize, height: redSize))
        redView.center = CGPointMake(CGRectGetMidX(view.layer.bounds), CGRectGetMidY(view.layer.bounds))
        redView.layer.cornerRadius = redSize / 2
        redView.layer.backgroundColor = UIColor.redColor().CGColor
        view.addSubview(redView)

        blueView = UIView(frame: CGRect(x: 0, y: 0, width: blueSize, height: blueSize))
        blueView.center = CGPointMake(0, CGRectGetMidY(view.layer.bounds))
        blueView.layer.cornerRadius = blueSize / 2
        blueView.layer.backgroundColor = UIColor.blueColor().CGColor
        view.addSubview(blueView)
        
        startSlidingBall()
    }
    
    func startSlidingBall() {
        let animation = CABasicAnimation(keyPath: "position.x")
        animation.toValue = NSNumber(float: Float(view.frame.size.width + blueView.frame.size.width))
        animation.duration = levelSpeed
        animation.autoreverses = false
//        animation.repeatCount = 100000
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        animation.delegate = self
        blueView.layer.addAnimation(animation, forKey: "move")
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isInside() {
            print("Dentro")
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.view.backgroundColor = UIColor.greenColor()
                }, completion: { (finished) -> Void in
                    self.view.backgroundColor = UIColor.whiteColor()
            })
            levelSpeed -= 0.10
            clickCount++
            scoreLabel.text = String(format: "Score: %i", arguments: [clickCount])
        }
        else {
            lives--
            livesLabel.text = String(format: "Lives: %i", arguments: [lives])
        }
        
        if lives == 0 {
            let alert = UIAlertController(title: "Touch game", message: "Game Over!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert) -> Void in
                self.resetGame()
            }))
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func resetGame() {
        clickCount = 0
        levelSpeed = 3.5
        lives = 5
        
        scoreLabel.text = String(format: "Score: %i", arguments: [clickCount])
        livesLabel.text = String(format: "Lives: %i", arguments: [lives])
        blueView.layer.removeAllAnimations()
        blueView.center = CGPointMake(0, CGRectGetMidY(view.layer.bounds))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func animationDidStart(anim: CAAnimation) {
        print(__FUNCTION__)
//        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "detectColision", userInfo: nil, repeats: true)
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        print(__FUNCTION__)
        startSlidingBall()
//        timer.invalidate()
    }

    func isInside() -> Bool {
        let presentationFrame = blueView.layer.presentationLayer()?.frame
        return CGRectIntersectsRect(redView.frame, presentationFrame!)
    }
    
//    func detectColision() {
//        let presentationFrame = blueView.layer.presentationLayer()?.frame
//        print(presentationFrame)
//        if CGRectIntersectsRect(redView.frame, presentationFrame!) {
//            print("Dentro")
//            view.backgroundColor = UIColor.blackColor()
//        }
//        else {
//            view.backgroundColor = UIColor.whiteColor()
//        }
//    }

}

